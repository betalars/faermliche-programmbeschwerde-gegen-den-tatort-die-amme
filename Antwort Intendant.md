# Die Antwort des HR-Intendant, Manfred Krupp

Hier ein Transcript der Antwort des Intendanten auf die förmliche Programmbeschwerde. Ich finde die Antwort okay, kann mich aber damit natürlich nicht zufrieden geben und werde daher die Beschwerde nun in die nächste Instanz, also an den Rundfunkrat, schicken.

*[DISCLAIMER:] die Antwort liegt mir nur in Papierform vor: dies ist ein Transcript und es besteht kein Anspruch darauf. Also wenn es hier kleinere Rechtschreibfehler gibt, sind vermutlich von mir...*

Sehr geehrter [betalars],

wir nehmen Bezug auf Ihre Programmbeschwerde über die Verbreitung des Films „Tatort: Die Amme" durch die ARD Degeto, die am 16. April 2021 unter anderem beim Rundfunkrat des Hessischen Rundfunks eingegangen ist. Zunächst mochte ich mich für Ihr Engagement bezüglich des Programms der ARD bedanken. Alle Verantwortlichen haben Ihre Beschwerde sehr ernst genommen.

Wie die Kolleg*innen des ORF und der ARD Degeto in mehreren Gesprachen und schriftlichen Rückmeldungen bereits formuliert haben, hatte niemand die Absicht, einzelne Personen oder gar ganze Gruppen zu kränken oder zu diffamieren. Insofern sind für uns all jene Wahrnehmungen, bei denen es zu solchen Krankungen kam, außerordentlich bedauerlich.

Wir können Ihnen zudem versichern, dass es nicht in der Absicht der Redaktion des ORF lag, den Täter als trans Person zu inszenieren, sondern das Bild eines gestörten und traumatisierten Menschen zu zeichnen. Insofern wurde eine spezifische Einordnung in Richtung des trans Spektrums aus unserer Sicht nicht vorgenommen.

Die Degeto hat im Vorfeld der Ausstrahlung den Film von der Jugendschutzbeauftragten des Hessischen Rundfunks prüfen lassen. Es konnten nach eingehender Prüfung keine für den Jugendschutz relevanten Themen festgestellt werden. Zusätzlich wurde auch das Kernteam Diversität des Hessischen Rundfunks, der Sitzanstalt der ARD Degeto, einbezogen. Dort konnte man bei diesem Tatort und der Ausarbeitung des Themas ebenfalls keine von Ihnen in der Beschwerde angesprochenen Auffälligkeiten feststellen.

Zudem hat sich die ARD Degeto im Vorfeld der Ausstrahlung im Ersten explizit Gedanken gemacht, wie der Film begleitet werden kann, um mögliche Fragen der Zuschauer*innen zum Film beantworten zu können. Die ARD-Zuschauerredaktion wurde mit entsprechenden Informationen zur Begleitung des Films auf dem Social Media-Kanalen ausgestattet

Das Thema Diversitat nehmen wir in unserer täglichen Arbeit in der ARD und im Hessischen Rundfunk sehr ernst. In einer Vielzahl unserer Programmbeiträge sind Integration und Vielfalt sichtbar und in Zukunft wird sich dieses Spektrum durch redaktionelle Steuerung noch erweitern. Wir beziehen uns in diesem Zusammenhang im Übrigen auf alle von der Gesellschaft marginalisierten Gruppen.

Die ARD Degeto ist zudem Mitglied der Charta der Vielfalt und hat mit einer Diversitätsbeauftragten in der Redaktion eine Anlaufstelle für das Thema Diversitat im Film. Sowohl der Hessische Rundfunkt als auch die Degeto sind bereits seit geraumer Zeit im Austausch mit diversen Gruppen und Organisationen, um unsere Arbeit kontinuierlich zu verbessern. So fand zum Beispiel erst vor wenigen Wochen ein virtuelles Treffen mit den Vertretern von #ActOut bei der Degeto-Redaktion statt.

Ein Ziel der ARD-Programmarbeit ist es, unsere bunte und vielfältige Gesellschaft in unseren Filmen und Serien abzubilden. Mit der Unterzeichnung der Charta der Vielfalt haben wir und die ARD Degeto uns außerdem dazu verpflichtet, kontinuierlich an einer Arbeitsumfeld zu arbeiten, das frei von Vorurteilen ist. Alle Mitarbeitenden erfahren Wertschätzung und Anerkennung - unabhängig von Geschlecht, Nationalitat, ethnischer Herkunft, Religion oder Weltanschauung, Behinderung, Alter, sexueller Orientierung und Identität.

Wir als Programmverantwortliche sind in jeder Angelegenheit mehr als interessiert daran, das Programm zu verbessern und geben bei jeder Entwicklung eines Filmstoffs unser Bestes, damit ein von Respekt getragenes Stück Unterhaltung entsteht. Schon allein aus diesem Grund sind eine gründliche Recherche und auch das Miteinbeziehen von Experten wichtig.

Ich darf Ihnen versichern, dass wir die von Ihnen geäußerte Kritik sehr ernst und auch zum Anlass nehmen, in Zukunft mit noch sensiblerem Blick Figuren und Geschichten für unsere Zuschauer innen zu entwerfen.

Mit freundlichen Grüßen

*gezeichnet*  
Manfred Krupp  
 \- Intendant -
