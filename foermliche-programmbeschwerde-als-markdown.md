## Betrifft: Programmbeschwerde über Verbreitung von "Tatort - die Amme" durch ARD Degeto

Ich beschwere mich hiermit förmlich beim Rundfunkrat des Hessischen Rundfunks über die Verbreitung von mutmaßlich jugendgefährdenden, gewaltverherrlichenden und volksverhetzenden Inhalten im Tatort "die Amme", erstmals in Deutschland ausgestrahlt am 28.03.21 in der ARD.

:::danger
**Content-Note:** Um diese Beschwerde zu formulieren, werden folgende Bereiche angesprochen und meistens auch thematisiert: Transfeindlichkeit, Diskriminierung von Menschen mit psychischen Störungen, Probleme in der mentalen Gesundheit und mit negativen Selbstbildern, suizidales Verhalten, Mobbing, Jugendgefährdung, schwere Kriminalität, schwere Gewalttaten, Gewalt gegen Kinder, Gewaltverherrlichung und Mord.
:::

Der Tatort "die Amme" ist dabei eine Produktion des ORF und wurde dort erstmals am 14.03.2021. ausgestrahlt. Gegen die Sendung wird sich derzeit in einem noch laufenden Verfahren vor dem Publikumsrat wegen Verstößen gegen 1.3.1., 1.3.2., 1.3.4., 1.3.5., 1.3.9., 1.4.4., 1.4.7., 1.6.3. und 1.7.1. ORF-Gesetz beschwert. (siehe Anhang)

Kern dieser Beschwerde jedoch ist die unkritische **Republikation** von fiktiven Darstellungen eines psychisch kranken Mannes, der sich als Frau verkleidet, so Zugang zu Opfern zu erlangt, schwerwiegende Gewaltverbrechen begeht, und dabei in der Rolle einer, wie er sich selbst bezeichnet, "Mutter" mehrere Kinder schwer misshandelt. Diese Darstellungen bestätigen zum einen die Rhetorik von transfeindlichen Gruppierungen, welche häufig genutzt wird um Grundrechte und Menschenwürde von trans Frauen anzugreifen. Darstellungen dieser Vorurteile unkritisch zu verbreiten ist nicht mit den Grundsätzen einer freiheitlich demokratischen Gemeinschaft vereinbar.

Darüber hinaus ist es für die Entwicklung queerer Jugendlicher extrem schädlich, mit transfeindlichen Inhalten und damit verbundener Diskriminierung konfrontiert zu werden. Erschwerend hinzu kommen stigmatisierende und entmenschlichende Darstellungen von psychischen Störungen, von denen queere Jugendliche als häufige Opfer von Ausgrenzung und Gewalt überdurchschnittlich häufig betroffen sind. Diesen Darstellungen im Zusammenhang mit unhinterfragter Rhetorik transfeindlicher Gruppierungen im abendlichen Fammilienprogramm der ARD eine Plattform zu geben ist beispiellos unverantwortlich und grausam gegenüber mehrerer stark marginalisierten Gruppen.

Diese Beschwerde stellt weiterhin fest, das das Programm selbst für Jugendliche, die nicht von queerfeindlichen Bewegungen bedroht werden, grundsätzlich ungeeignet ist. Diese Einschätzung teilt auch der ORF, der das Programm als für Jugendliche unter 16 Jahren für ungeeignet bewertet. Deutschen Leitfäden für Jugendschutz folgend kommen die Beschwerdeführenden zu der Einschätzung, dass das Programm Indizien für eine Alterseinstufung ab 18 Jahren enthält.

Hinzu kommen auch Verstöße gegen Grundsätze zur Vermeidung von Gewaltverherrlichung im Fernsehen.

Das Programm wurde zwar nicht durch den hessischen Rundfunk *produziert*, jedoch ist die ARD Degeto GmbH als Tochterfirma des HR für die Verbreitung verantwortlich. Daher richtet sich diese Beschwerde an den HR-Rundfunkrat.

## Rechtsgrundlagen und Programmgrundsätze
Die Beschwerde stellt Verstöße gegen folgende Programmgrundsätze fest:

### [Verstöße gegen das HR-Gesetz](https://download.hr.de/unternehmen/rechtliche-grundlagen/2017-hr-gesetz-100.pdf)
>§3 3. Die Darbietungen dürfen nicht gegen die Verfassung und die Gesetze verstoßen oder das sittliche und religiöse Gefühl verletzen. Sendungen, die Vorurteile oder Herabsetzungen wegen der Nationalität, Rasse, Farbe, Religion oder Weltanschauung eines einzelnen oder einer Gruppe enthalten, sind nicht gestattet.

Der Täter entspricht böswilligen Vorurteilen gegenüber trans Frauen. Das Programm würdigt Menschen mit psychischen Störungen und trans Frauen herunter.

### Verstöße gegen [Grundsätze für die Zusammenarbeit im ARD-Gemeinschaftsprogramm "Erstes Deutsches Fernsehen" und anderen Gemeinschaftsprogrammen und -angeboten:](https://www.ard.de/download/1899726/Grundsaetze_fuer_die_Zusammenarbeit_im_ARD_Gemeinschaftsprogramm_.pdf/) bzgl. Programmgestaltung
**(1) Auftrag**
> (b) Die Programme und Angebote der ARD dienen der Information, Bildung, Beratung und Unterhaltung. Die Angebote und Programme der ARD haben ein vielfältiges kulturelles Angebot zu vermitteln. Sie berücksichtigen die Bedürfnisse von Mehrheiten und Minderheiten.

> (g) Die ARD vermittelt und fördert Kultur, Kunst und Wissenschaft. Das Geschehen in den Ländern und die kulturelle Vielfalt der Bundesrepublik sind in den Programmen und Angeboten der ARD angemessen darzustellen. [...]

In Tatorten im Speziellen, aber auch im gesamten Fiction-Bereich der ARD werden transgender Identitäten fast nie thematisiert. Männer in Frauenkleidern hingegen schon: "Die Amme" ist bei weitem nicht das einzige Beispiel, welches dieses böswillige mediale Klischee repliziert.

Es entsteht der Eindruck es gäbe fast keine trans Frauen, dafür jedoch viel Kriminalität durch Männer, die sich als Frauen ausgeben.

So verstärkt das Programm negative Vorurteile über trans Frauen, unterstützt die Rhetorik transfeindlicher Gruppierungen, was Interessen und Menschenwürde von trans Frauen nicht nur ignoriert, sondern aktiv angreift. Das fiktionale Programm versagt systematisch darin die Geschlechtervielfalt und die damit verbundene queere Kultur in Deutschland abzubilden.

**(2) Anforderungen an die Gestaltung von Sendungen und Angeboten**
> (a) Die Würde des Menschen ist zu achten und zu schützen. [...] Die Programme und Angebote sollen dazu beitragen, die Achtung vor Leben, Freiheit und körperlicher Unversehrtheit zu stärken.

>(c\) Die Programme und Angebote sollen die Toleranz im Sinne der Achtung von Glauben, Meinung und Überzeugung der Mitmenschen fördern. Die sittlichen und religiösen Überzeugungen der Bevölkerung sind zu achten. 

Die transfeindlicher Rhetorik entsprechenden Darstellungen werden häufig genutzt, um die Würde von trans Frauen durch Verächtlich machen anzugreifen. Dadurch motiviert wird systematische Ausgrenzung von trans Frauen in breiten Teilen der Bevölkerung, aber auch schwere Gewaltverbrechen und sexuelle Übergriffe.

> (d) Dem Schutz der Jugend ist besondere Aufmerksamkeit zu schenken. Hierfür gelten in Ergänzung der gesetzlichen Vorschriften die ARD-Richtlinien zur Sicherung des Jugendschutzes und die ARD Kriterien zur Sicherung des Jugendschutzes. Gewalt darf nicht verharmlost oder verherrlicht werden. In den Programmen und Angeboten der ARD werden keine indizierten Filme ausgestrahlt. Die Anliegen von Familien und Kindern sowie der Gleichberechtigung von Frauen und Männern sind angemessen zu berücksichtigen. 

Das Programm ist gefährlich für die Entwicklung queerer Jugendliche und ist gemäß des Jugendschutz-Staatsvertrages unzulässig.

Unabhängig davon sind jedoch bereits wegen reißerischen, immersiven und intensiven Darstellungen von Gewalt gegen Kinder für Minderjährige ungeeignet.

Das Programm verstößt in vielen Punkten gegen Grundsätze gegen Gewaltverherrlichung im Fernsehen und weist nach dem Jugend-Medienschutz Leitfaden Indizien für eine Alterseinstufung ab 18 auf.

Das Programm berücksichtigt das Anliegen von trans Frauen auf Gleichberechtigung nicht.

### Verstöße gegen den [Jugendschutz-Staatsvertrag](https://download.hr.de/unternehmen/struktur/jugendmedienschutz-staatsvertrag-102.pdf)
Angebote sind gemäß §4 (1) Jugendschutz-Staatsvertrag unzulässig, wenn sie
> 3. zum Hass gegen Teile der Bevölkerung [...] aufstacheln, [...] oder die Menschenwürde anderer dadurch angreifen, dass Teile der Bevölkerung oder eine vorbezeichnete Gruppe beschimpft, böswillig verächtlich gemacht oder verleumdet werden,
> 5. grausame oder sonst unmenschliche Gewalttätigkeiten gegen Menschen in einer Art schildern, [...] die das Grausame oder Unmenschliche des Vorgangs in einer die Menschenwürde verletzenden Weise darstellt; [...]

Das Programm zeigt detailliert unmenschliche körperliche und mentale Gewalt gegen Kinder, die durch einen Täter mit psychischen Störungen verübt werden, der negativen Zuschreibungen über trans Frauen entspricht. Der Täter wird dabei als entmenschlichtes Monster verunglimpft. Entgegen von genreüblichen Erzählstrukturen wird ihm nicht einmal ein Motiv zugeschrieben. Setting und Darstellung wirken immersiv und realitätsnahe welches Zuschauende packt und deren Weltanschaung durch die sehr ernst und differenziert wirkende Handlung im besonderen Maße prägt.

Wie effektiv diese und vergleichbare Darstellungen im Verächtlich machen von trans Frauen sind, ist daran ersichtlich, wie häufig vergleichbare Rhetorik genutzt wird, um die Menschenwürde und -rechte von trans Frauen anzugreifen.

Programme sind gemäß §4 (2) Jugendschutz-Staatsvertrag weiterhin unzulässig, wenn sie ...
 > 3. offensichtlich geeignet sind, die Entwicklung von Kindern und Jugendlichen oder ihre Erziehung zu einer eigenverantwortlichen und gemeinschaftsfähigen Persönlichkeit unter Berücksichtigung der besonderen Wirkungsform des Verbreitungsmediums schwer zu gefährden. 

Eine offensichtliche Jugendgefährdung wird in dieser Beschwerde ebenfalls aufgezeigt.

## Erklärung der Verstöße
Die Beschwerde moniert das unkritische Weiterverbreiten des Medialen Klischees von psychisch gestörten, kriminellen cis Männern in Frauenkleidern. Hierbei handelt es sich um ein transfeindliches Narrativ, das seit Jahren immer wieder vehement von Menschenrechtsaktivist_innen verurteilt wird.

Da der Tatort "die Amme" diese Rhetorik weiterverbreitet, gab es viel Bestürzung der trans Community, die sich anhand von Twitter gut nachvollziehen lässt. Unter dem Hashtag [#tatort](<https://twitter.com/search?lang=de&q=(%23tatort) until%3A2021-03-30 since%3A2021-03-27>), aber auch als Antwort auf [offizielle Postings der zuständigen Social Media Redaktion](https://twitter.com/Tatort/status/1376258720867434497) haben viele trans Accounts und deren Allys diese Darstellungen verurteilt und über erfahrenes Leid berichtet.

Das scheinbar durch die Kritik motivierte [Statement der Tatort-Redaktion](https://twitter.com/Tatort/status/1376447308930383877?s=20):
> Es war nicht Anliegen der Kreativen, die Geschichte eines transidenten Täters zu erzählen. Die Rolle wurde mit Attributen versehen, die von einer Transperson abgrenzen. Die Kommissar*innen gingen von einer Tat mit pädophilem Hintergrund aus, was eine Kindesentführungen nahelegt.

Diese Darstellung schreibt trans Aktivist_innen eine Aussage zu, die so nie getroffen wurde. Den Beschwerdeführenden war nicht möglich auch nur einen "own-voice" Tweet zu finden, der moniert, dass die Tatperson eine trans Frau sei.

Die Beschwerdeführenden empfehlen deshalb dem Rundfunkrat grundsätzlich die Kompetenz und kognitive Gesundheit von trans Frauen nicht anzuzweifeln. Trans Frauen können erkennen, wann eine trans Frau im Fernsehen dargestellt wird und wann nicht. Trans Frauen sind in der Lage "cis Mann" und "trans Frau" sowohl begrifflich als auch konzeptionell voneinander zu trennen. Trans Frauen sind aufgeklärt darüber, welche medialen Darstellungen systematisch dafür genutzt werden ihre Menschenrechte anzugreifen. Ihre Beschwerden sollten diesbezüglich grundsätzlich ernst genommen werden - insbesondere, weil sie als Betroffene sehr differenzierte Kenntnisse über Transfeindlichkeit entwickeln *müssten*, die unter vielen Medienschaffenden jedoch nicht weit genug verbreitet sind. Und viele trans Frauen sind zudem auch solidarisch mit den Interessen von Menschen mit psychischen Störungen, die durch den Tatort "die Amme" gleichermaßen verächtlich gemacht werden.

So verurteilt diese Beschwerde den manipulative und ignoranten Tweet des Tatort-Accounts ausdrücklich, leistet dennoch nun Aufklärungsarbeit darüber, inwieweit Darstellungen von Männern trans Frauen verächtlich machen können.

In Film, Fernsehen und Literatur haben Darstellungen von Männern, die sich aus negativen Intentionen heraus Frauenkleider anziehen, und so lächerlich oder verächtlich gemacht werden, eine lange Tradition. Darüber berichtet beispielsweise sehr ausführlich [die Netflix-Serie "Disclosure"](https://www.netflix.com/de/title/81284247) Erste Beispiele dieses Vorurteils lassen sich bereits in frühen Stummfilmen nachweisen. Das Klischee ist dabei auch heute immer noch noch weit und findet sich sogar in Kinderprogrammen der Öffentlich Rechtlichen wieder - beispielsweise in der Löwenzahn-Folge ["Spiegel - die Tückische Täuschung"](https://www.zdf.de/kinder/loewenzahn/spiegel-die-tueckische-taeueschung-100.html)

Zu international bedeutsamen Beispielen dieses Klischees zählen "Das Schweigen der Lämmer" (1991), "Dress to kill" (1980), "House at the End of the Street" (2012), "Psycho" (1960), "Blutiger Sommer" (1983) oder „Troubled Blood“.

Die Botschaft der Werke - traue niemals einem Mann in Frauenkleidern - ist aus Perspektive von trans Frauen dabei immens problematisch:

Eine Frau ist trans, wenn sie zur Geburt ein anderes Geschlecht zugeschrieben bekam, meistens das männliche. Ohne medizinische Intervention bilden ihre Körper Geschlechtsmerkmale aus, die der wahrgenommenen Identität widersprechen. Dies ist Ursache von Geschlechtsdysphorie, die unbehandelt zu weiteren psychischen Störungen und sehr häufig auch suizidalem Verhalten führt. Das medizinische Angleichen von (bei trans Frauen) maskulinen Merkmalen des Körpers an die weibliche Geschlechtsidentität ist ein heilender Vorgang, für den es jedoch kaum entsprechend positive mediale Bilder gibt.

So stellt auch der Tatort "die Amme" die Transformation des Täters hin zu einer "Mutter" als ein krankhaftes Symptom einer psychischen Störung dar, [was für reale Menschen ein Trigger für Geschlechtsdysphorie sein kann](https://twitter.com/doing_fenja/status/1376269158015438848).

Dabei sind trans Frauen während ihrer Transition nicht nur besonders verletzlich, weil sie oftmals Geschlechtsdysphorie zu Beginn einer Transition am stärksten wahr nehmen … sie wirken auf andere hier oft noch maskulin. Ziehen sie nun als Ausdruck ihrer Geschlechtsidentität ein Kleid an, können sie wie ein Mann in Frauenkleidern wirken.

Dass die medial konditionierte erste Assoziation vieler Menschen hier: "psycho-Killer" und nicht "trans Frau zu Beginn eines heilenden Prozesses" ist, verängstigt trans Frauen und motiviert Gewaltverbrechen.

Das alles ist schon Grund genug aus Rücksichtnahme und Menschlichkeit auf Darstellungen von bösen Männern in Frauenkleidern grundsätzlich zu verzichten. Das Narrativ wird jedoch auch systematisch genutzt, um die Rechte und Würde von trans Frauen anzugreifen.

Denn transfeindliche Gruppierungen erkennen die Existenz von trans Frauen grundsätzlich nicht an. Für sie entsprechen trans Frauen nicht nur medialen Bild von psycho-Killern in Frauenkleidern, sie sind dieser Ideologie zufolge nichts anderes als Männer, die sich das weibliche Geschlecht aus niederen Beweggründen aneignen.

Mediale Darstellungen, die dieser Lüge entsprechen, sind Wasser auf die Mühlen dieser menschenverachtenden Gruppierungen.

### Verstöße gegen [Grundsätze zur Vermeidung von Gewaltverherrlichumg im Fernsehen](https://www.ard.de/download/564052/ARD_Grundsaetze_gegen_die_Verharmlosung_und_Verherrlichung_von_Gewalt_im_Fernsehen.pdf) in den Punkten B-2.1. und B-2.2.
Dabei ist die Sendung nicht nur potentiell dazu geeignet, Gewalt gegen trans Frauen zu motivieren, es wird auch gegen Grundsätze zur Vermeidung von gewaltverherrlichenden Darstellungen verstoßen. Die replizierten stereotypen Handlungsmuster von kranken Gewaltverbrechern in Frauenkleidern haben eine verrohende Wirkung und sind mit Grundsätzen von solidarischem Zusammenleben in einer der Menschenwürde verpflichteten Gesellschaft nicht vereinbar.

Das Ausmaß an roher psychischer und physischer Gewalt gegen Kinder wird durch eine ansonsten künstlerisch nur sehr oberflächlich entwickelte Täterfigur in keiner Weise aufgewogen.

Darüber hinaus fördert das Angebot die Degradierung und Diskriminierung von trans Frauen und Menschen mit Traumafolgestörungen.

### Verunglimpfende Darstellungen von Gewalt durch Menschen mit psychischen Störungen
Das Programm ist selbst unter der falschen Annahme es würde trans Frauen nicht betreffen, allein wegen der Darstellung von Traumafolgestörungen und Suchtstörungen immens problematisch.

Zum einen ist der Täter hochgradig absurd und zeugt von einem sehr laienhaften, durch viele mediale Vorurteile verzerrtem Verständnis psychischer Störungen. Solche Darstellungen sind zwar sehr publikumswirksam und dazu geeignet hohe Einschaltquoten zu erzielen, vermitteln jedoch keine Empathie für die zum Teil sehr prekären Lebensumstände von realen Personen. Dabei fehlen ganz grundsätzlich mediale Darstellungen von Menschen mit psychischen Erkrankungen oder sog. Störungen, die beispielsweise kompetent und selbstständig ihren Alltag gestalten. Selbst scheinbar "positive" Darstellungen (Professor T., Ella Schön o.ä.) verzerren, überzeichnen, reduzieren auf Stereotype und verfehlen häufig die Lebensrealitäten der dargestellten Personengruppen.

Dabei gibt es im gesamten fiction-Bereich der ARD allgemein, jedoch auch in Tatorten im speziellen immer wieder Darstellungen, die gesellschaftlich nicht akzeptierte Coping-Mechanismen und (teils reale, teils auch frei erfundene) Verhaltensmuster von Menschen mit psychischen Störungen als etwas lächerliches, seltsames oder in diesem Fall sogar abartiges darstellen.

Dabei werden Menschen, die von psychischen Störungen betroffen sind, in Deutschland weit verbreiteter und von breiten Teilen der Bevölkerung unterstützer Diskriminierung ausgesetzt. Die ungerechtfertigte Diskriminierungen von Menschen, die in ihrem Leben einmal Unterstützung aufgrund von psychischen Störungen gesucht haben, ist nicht nur weitgehend akzeptiert, sondern teilweise auch formell vorgeschrieben.

Ursächlich dafür sind verbreitete Stigma, die Betroffenen Kriminalität, Gewalttätigkeit, Kaltblütigkeit, Unberechenbarkeit und viele weitere negative Eigenschaften zuschreiben. Dabei wurden positive Korrelationen zwischen dem Konsum von unsachlichen, sensationsgetriebenen medialen Darstellungen von Gewalt durch Menschen mit psychischen Erkrankungen und der Verbreitung von gegen diese Menschen diskriminierendem Gedankengut bereits 1980 wissenschaftlich aufgezeigt.

Der Tatort “die Amme” stellt abstoßende Gewalttaten durch eine Person mit psychischen Störungen intensiv dar. In diesem Programm ist dabei die Bestätigung von stigmatisierendem Gedankengut besonders schwerwiegend:

Zum einen wird über trans Frauen (und trans Personen im Allgemeinen) häufig grundsätzlich behauptet ihre Identität sei lediglich eine psychische Krankheit, zum anderen entwickeln sie in der Folge der erlebten Diskriminierungen und Stigmatisierungen zum Teil Depressionen, Sozialphobien, Anspassungsstörungen und weitere psychische Erkrankungen.

Da sowohl psychische Störungen als auch queere Identitäten gesellschaftlich stigmatisiert werden, fällt es Betroffenen hier oftmals schwer, therapeutische Begleitung, Behandlung oder medizinische Versorgung in Anspruch zu nehmen . Programme, die diese Mechanismen unterstützen sind so im besonderen Maße unmenschlich und insbesondere jugendgefährdend. Auf die überdurchschnittliche Suizidalität von Jugendlichen, aber auch Erwachsenen sei an dieser Stelle verwiesen: Sie ist im Längsschnitt erforscht und belegt. Verzerrte und negative mediale Darstellungen fördern Alltagsdiskriminierung, familiäre und gesellschaftliche Ausgrenzung und Internalisierung von (Selbst-)Ablehnung und (Selbst-)Entwertung.

### Jugendgefährdung durch Transfeindlichkeit

Trans Jugendliche, deren Geschlechtsidentität durch Erziehungsberechtigte, Lehrkräfte und Gleichaltrige nicht anerkannt wird, entwickeln in der Hälfte der Fälle suizidales Verhalten. Diese Jugendliche mit transfeindlichen, stigmatisierenden und Kriminalität zuschreibenden medialen Inhalten zu konfrontieren, löst unbeschreibliches Leid aus, ist kaltherzig und verantwortungslos.

Trans Jugendliche werden durch das Programm darauf konditioniert, Abscheu vor der eigenen Identität zu entwickeln. Ihnen wird vermittelt, dass sie gesellschaftlich von vielen als abscheulich, kriminell oder unnatürlich wahr genommen werden und eine Gefahr für andere darstellen. Die einzigen medialen Orientierungspunkte, die diese Kinder haben sind Figuren, die entweder lächerlich oder verächtlich gemacht werden.

Das sorgt auch dafür, dass Eltern davor zurück schrecken die Identität ihrer Kinder anzuerkennen. Schließlich möchten sie nicht, dass ihr "Junge" (die eigentlich ein Mädchen ist) so wird, wie die Psychiokiller im Tatort. Dann auch noch zu akzeptieren, dass das eigene Kind eine psychische Störung entwickelt haben könnte, kommt für viele dem Eingestehen von schwerwiegenden Versagen als Eltern gleich. So machen sie trans Kindern den Zugang zu professioneller Hilfe nicht nur dringlicher, sondern auch schwerer.

Dabei sind queere Kinder auch häufig von Mobbing in der Schule betroffen. Sendungen wie der Tatort geben Mobbern neue Vorwände, ihre Mitschüler_innen zu erniedrigen.

### Grundsätzliche Uneignung des Programms für Jugendliche
[ARD-Kriterien zur Sicherung des Jugendschutz](https://download.hr.de/unternehmen/struktur/ard-kriterien-sicherung-jugendschutz-100.pdf) sowie dem [Jugend-Medienschutz-Leitfaden](https://download.hr.de/unternehmen/struktur/jugendmedienschutz-leitfaden-100.pdf) folgend geht dieses Beschwerde davon aus, dass das Programm grundsätzlich für jugendliche ungeeignet ist und potentiell als jugendgefährdend einzustufen ist.

Sonntag, 20:15 in der ARD ist ein Sendeplatz, zu dem viele Familien gemeinsam fern sehen. Sendungen, die in dieser Zeit platziert werden, sollten so mindestens für Kinder ab 12 Jahren geeignet sein. Dass der Tatort "die Amme" dieses Kriterium nicht erfüllt, schätzt der ORF selbst so ein: die Sendung sei nach der ORF-Mediathek für Jugendliche unter 16 Jahren ungeeignet.

Der Film ist anhaltend spannungsgeladen und schaurig. Es gibt viele Szenen, in denen mentale und körperliche Gewalt gegen ein entführtes Kind ausführlich dargestellt wird: seine Mutter wird ermordet, er wird gefesselt, geknebelt, für Fluchtversuche und Kleckern erniedrigt, zu körperlicher Nähe gegen seinen Willen gezwungen und durch in Aussicht gestellte Belohnungen wird versucht ihn manipulativ zu konditionieren. Dies bereits ist für Kinder verängstigend und verstörend. 

Des weiteren wird jedoch auch das Schicksal eines weiteren Jungen immer wieder angedeutet und nie aufgeklärt.

Durch das Weglassen von Informationen ist dieser Handlungszweig für Kinder und Jugendliche vermutlich umso verstörender: Denn, was dem Kind alles schreckliches in der Gefangenschaft passiert, malen sich zusehende Kinder in ihrer Fantasie aus. Angeregt durch die konstant immersiv/düstere Bild- und Tonkulisse werden Horrorszenarien aufgebaut, die teils noch viel belastender sein können, als die so schon krassen expliziten Darstellungen von Gewalt.
Erschwerend hinzu kommt, dass auch in vermeintlich entspannenden Szenen intensiv thematisiert wird, dass die ermittelnde Kommissarin durch das Schicksal der Kinder keinen Schlaf finden kann.

Aber es ist nicht nur problematisch, weil es so verstörend ist: Kinder und Jugendliche orientieren im Bezug auf ihr moralisches Handeln an den Medien - insbesondere wenn Themen im Schulunterricht nicht behandelt werden. Da im Programm keine kritische Einordnung der transfeindlichen Rhetorik statt findet, ist davon auszugehen, dass die Sendung Weltbilder von Kindern und Jugendlichen negativ geprägt hat. Es ist zu vermuten, dass bei Kindern durch die Assoziation mit den Gewaltdarstellungen ein Unwohlsein gegenüber trans Frauen ausgebildet wird, die ähnlich wie der Täter oberflächlich maskuline Eigenschaften aufweisen können.

Die dramatischen, realistisch in Szene gesetzten Darstellungen verstören Kinder und Jugendliche gleichermaßen und desorientiert sie sozialethisch durch eine Abwertung von trans Frauen.

Die äußerst drastischen und intensiven Gewaltdarstellungen, das Ernidrigen der Prostituierten durch den Täter, der extreme explizite Sexismus, die implizite Transfeindlichkeit und die Verunglimpfung von trans Frauen spricht dafür, dass das Programm für Jugendliche unter 18 Jahren ungeeignet ist. Erschwerend hinzu kommt, dass trans Mädchen aufgrund negativer gesellschaftlicher Zuschreibungen sich zu einer Identifikation mit dem Täter genötigt fühlen können, was Geschlechtsdyspherie verstärken kann und Angst davor macht, die eigene Identität zu entwickeln und auszuleben.

Der Film ist besonders immersiv inszeniert, spielt in einem realitätsnahen Setting, ist verglichen zu anderen Tatorten genreuntypisch, hat fast keine entspannenden Szenen, wenig Humor, ein sehr offenes Ende und erfüllt auch insbesondere durch die starke Nähe, die zu den filmischen Figuren aufgebaut wird, fast keine der in der Alterseinstufung relativierenden Faktoren.

##ergänzende  Anmerkungen zu queer Kultur
Während diese Beschwerde sich inhaltlich vorwiegend auf binäre trans Frauen und queere Jugendliche konzentriert hat, sind sie bei weitem nicht die einzigen Gruppen, aus deren Perspektive das Angebot problematisch ist. In der notwendigen Differenziertheit alle Diskriminierungsvektoren aufzuzählen ist in einer Programmbeschwerde kaum sinnvoll möglich und so besteht kein Anspruch auf Vollständigkeit.

Es ist zu jedoch betonen, dass Darstellungen zum Beispiel auch aus der Perspektive von Crossdresser_innen, nichtbinären Personen, Eltern von trans Kindern und auch queeren Gemeinschaften ganz allgemein problematisch sind. Angehörige dieser Gruppen haben dies ihren Erklärungen, warum sie mitzeichnen möchten, auch dargelegt. Nicht nur sind viele davon direkt durch die hier dargelegte Diskriminierung betroffen, es wurden ihnen gegenüber auch indirekt unsichtbare Barrieren verstärkt:

Wo eine Subgruppe der LGBTQI+ Bewegung verächtlich gemacht, können sich viele andere Menschen dieser Gemeinschaft nicht mehr sicher fühlen. So hindern Beiträge wie „die Amme“ ganz grundsätzlich das Abbilden der Vielfalt bezüglich Geschlechtsidentitäten, Geschlechtsauslebung und sexuellen Orientierungen im öffentlich-rechtlichen Rundfunk.

Im übrigen lassen sich selbstverständlich auch nicht alle hier getroffenen Aussagen auf jede trans Frau verallgemeinern. Geschlechtsangleichende Maßnahmen anzustreben ist zum Beispiel keine Voraussetzung dafür eine trans Frau zu sein! Und für die meisten trans Menschen sind auch viele Geschlechtsmerkmale deutlich weniger binär, als es gesellschaftliche Normen vorgeben.

## Zusammenfassung/Einordnung 
Die Stigmatisierung von Menschen mit psychischen Störungen, die intensiven Darstellungen von Gewalt gegen Kinder, die Gefährdung, die dieses Programm insbesondere für die Entwicklung queerer Jugendliche darstellen könnte sowie die schwerwiegende Transfeindlichkeit - das alles hätte für sich allein gestellt bereits die Republikation der Sendung in der ARD verhindern müssen. Zumal sowohl ORF als auch ARD nach der Erstaustrahlung des Tatorts in Österreich umgehend über transfeindliche Motive der Sendung aufgeklärt wurden. Die förmliche Beschwerde wegen Verstößen gegen das ORF-Gesetz lag der ARD Degeto zum Zeitpunkt der Ausstrahlung vor.

Trotzdem wurde das Programm gesendet und es ist davon auszugehen, dass viele der in dieser Beschwerde aufgezeigten Verstöße gegen Programmgrundsätze die negative Auswirkung des Programms auf die trans Community verstärkt haben.

Dabei ist die Lage von trans Rechten in der aktuellen Weltpolitischen Lage besonders brisant: Fortschritte in Sichtbarkeit und Anerkennung von transgender Identitäten werden begleitet von immer häufigeren politischen Angriffen auf Grundrechte und tätlichen Übergriffen auf trans Menschen.

Die Beschwerdeführenden verweisen in Deutschland auf eine [Mediale Hetzjagd nach einer Trans Frau in Freiburg](https://fluss-freiburg.de/offener-brief-zur-berichterstattung-vom-30-7-2020-der-badischen-zeitung-fudder-dem-presseportal-der-polizei-freiburg/), die einfach nur mit einem Kind an einem Spielplatz gesehen wurde.

Wahrung und Schutz von trans Rechten hilft ein starker öffentlich-rechtlichen Rundfunk, der bei Angriffen auf die Menschenwürde hinschaut und im Bezug auf transfeindliche Muster sensibilisiert ist. Leider wurde hier jedoch über Verstöße gegen Jugendschutz und die Interessen von Menschen mit psychischen Störungen hinweg gesehen, um ein Programm zu rechtfertigen, das letztendlich ein weit verbreitetes, veraltetes und lang schon als transfeindlich erwiesenes Motiv aufwärmt.

Das zumindest ist die Botschaft, die das Programm vielen trans Personen vermittelt hat, was viel Vertrauen in die Öffentlich-Rechtlichen Medien beschädigt hat und den Beitrag der Anstalten für Verständigung und sozialen Frieden sabotiert.

Dabei ist den Beschwerdeführenden durchaus bewusst, dass es bei Programmverantwortlichen intern Sensibilisierung und Empathie für die Probleme von trans Frauen gibt. Und das nicht nur, weil es erfreulich viel Gesprächsbereitschaft gab, nachdem unförmliche Hinweise auf Verstöße gegen Programmgrundsätze zur ARD Degeto vorgedrungen sind.

Das interne Verständnis für vorgebrachte Programmkritik sowie Unzufriedenheit über problematische Motive wäre jedoch ohne uneigennützige Kommunikationsarbeit der Beschwerdeführenden nicht an die Öffentlichkeit gelangt. Was dagegen spricht Verständnis und Empathie für die Beschwerden von trans Frauen auch über offizielle Kommunikationskanäle der ARD zu vermitteln, ist sicherlich nur eine von vielen Fragen, die dieser Vorgang aufwirft.

Das Stellen und Beantworten dieser Fragen wird nun vermutlich die Aufgabe von Rundfunkrat und Programmausschuss sein. Wird dieser Beschwerde Recht gegeben, sollte dabei auch überlegt werden, wie es in Zukunft besser geht.

Im Interesse der Versöhnung hier einige Vorschläge, wie Vertrauen wieder aufgebaut werden kann:
 - Sich klar und öffentlich in Rücksprache mit Betroffenen von der Sendung zu distanzieren wäre ein erster Schritt,
 - die gezielte Aufklärung von Zuschauenden und Mitarbeitenden über Transfeindlichkeit und Diskriminierung von Menschen mit psychischen Störungen in Gesellschaft und Medien ein zweiter.
 - Doch queere Menschen brauchen nicht nur Aufmerksamkeit für ihre Probleme - die kennen wir selbst gut genug! Wir brauchen vor allem auch positive Charaktere, in denen wir uns als selbstverständlicher Teil einer diversen Gesellschaft wiedererkennen können.
 - Hierfür notwendig ist ein aktives Entgegenkommen auf queere Communities, da viele unsichtbare Barrieren deren Kultur aus Rundfunkanstalt und Programm fernhalten.
 - Dabei sollte auch insbesondere das Kinderprogramm berücksichtigt werden. Kinder wissen häufig früh schon, wenn ihnen zur Geburt das falsche Geschlecht zugewiesen wurde. Leider fehlen hier oft mediale Vorbilder, richtige Worte und verständnisvolle Erziehungsberechtigte.

So verbleiben die Beschwerdeführenden in der Hoffnung, dass der hessische Rundfunk sich durch diese Mahnung zu einer offeneren Fehlerkultur und einem bunteren Programm ermutigen lässt, was die Rolle der Anstalt zum Erhalt unserer demokratischen Grundwerte nur stärken kann.

mit freundlichen Grüßen,
betalars

Mitgezeichnet durch:
*(wenn hier alle hincommitten, wird das Chaos! Bitte guckt in den Ordner Mitzeichnende rein, kopiert das Template und pusht da ne neue Datei rein!)*
