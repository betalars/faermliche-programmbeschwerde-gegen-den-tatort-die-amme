# Muster

Ich erkläre hiermit, dass ich die förmliche Beschwerde an den Rundfunkrat des hessischen Rundfunks gegen die Republikation des Tatortes "die Amme" mitzeichnen möchte.

Hier gebe ich Gründe dafür an, damit das nicht mit Bots gecheated werden kann.

 - [ ] ich möchte unter dem Namen *Franziska Nord* genannt werden
 - [ ] ich möchte unter dem Pseudonym *Fnord* genannt werden
 - [ ] ich möchte anonym zu weiteren Mitzeichnenden gezählt werden
 - [ ] ich möchte, dass die Begründung meiner Unterstützung der Beschwerde angehängt wird
 - [ ] [ich weiß der hessische Rundfunk hat eine Datenschutzerklärung](https://www.hr.de/datenschutz/index.html) und wenn ich bei der Beschwerde mit Namen, Pseudonym oder Texten mitmachen will, werden da dann möglicherweise personenbezogene Daten von mir hin übermittelt. Außerdem ist mir aufgefallen, dass dieses gibtlab repository öffentlich ist und ich verstehe, dass hier Internet is und so.
