# Was ist dieses Repository?
Hier werden zur besseren öffentlichen Nachvollziehbarkeit Dokumente veröffentlicht, die für die Formulierung einer förmlichen Programmbeshwerde gegen die Weiterverbreitung des Tatortes "die amme" durch die ARD Degeto relevant sind.

## Wie kann ich da mitmachen?

Gerade nicht mehr. Du kannst natürlich gern auch auf meiner Beschwerde aufbauend einen eigenen Brief an den hessischen Rundfunk formulieren aber darüber hinaus kann mir derzeit nicht untersützt werden.

## Und was machen wir jetzt?
Wir warten. Ich kann bestätigen, dass Menschen beim HR die Beschwerde gelesen haben. Theoretisch wird das Verfahren jetzt angleihert werden, wenn irgendwas formell dagegen spricht, wird mir hoffentlich noch Bescheid gegeben werden. Der Intendant wird mir demnächst auf meine Beschwerde antworten und sollte ich damit nicht glücklich sein, kommt das ganze vor den Rundfunkrat. 

## Was ist eine Programmbeschwerde?
Öffentlich-Rechtliche sind zum einen in Deutschland überwiegend juristisch immun, werden jedoch auch von der Gemeinschaft bezahlt. Verstoßen sie gegen die dadurch begründeten Programmgrundsätze, können sich Zuschauende bei unabhängigen Kontrollinstanzen, den Rundfunkg- bzw Fernsehräten beschweren.

## Und ist das alles jetzt faktisch richtig, was da steht?
Ich kann versichern, dass ich mir Argumente nicht aus den Fingern gesaugt habe und als nichtbinäre Person auf dem autistischen Spektrum von ganz viel Diskriminierung, die da in diesem meiner Meinung nach abscheulichen Tatort verbreitet wurde, selbst betroffen bin. Dennoch bin ich natürlich voreingenommen, weil es meine Beschwerde ist. Aussagen haben alle eine faktsiche Grundlage, sind jedoch nicht unbedingt alleinstehend zitierfähig. Mit anderen Worten: ihr dürft das nutzen um euch ne Meinung zu bilden und, um über den Vorgang zu berichten. Das ganze eignet sich allerdings nicht als faktischer Beleg zum Beispiel für die Aussage, dass das Programm der ARD sei jugendgefährdend. Das ist erstmal meine Behauptung, die zunächst durch Intendant und schließlich auch den Rundfunkrat geprüft werden muss.

Wenn ihr also ohne die in dieser Beschwerde vorhandene Differenziertheit einfach auf Basis dieser Beschwerde beispielsweise behauptet der Tatort "die Amme" sei objektiv volksverhetzend gewesen ... wundert euch nicht, wenn ihr vom HR verärgerte Post bekommt.

## Das ist alles total heftig, arbeiten bei der ARD nur Unmenschen?
Ich denke nicht. Ich kann sogar darüber berichten, dass ich einige Mitarbeitende der ARD-Gruppe, die ich persönlich kenne, mich für meine Programmkritik schätzen. Programmverantwortliche für die Ausstrahlung des hier monierten Tatortes in Deutshcland haben sogar schon mit mir telefoniert und waren da mit dem Programm auch in einigen Teilen unglücklich.

Außerdem: wenn ich ehrlich denken würde die wären alle fies, dann würd ich mir den ganzen Aufwand hier auch nicht machen.

Dennoch: die Sendung war halt nicht okay und wenn wir demokratische Kontrollmechanismen nicht nutzen, gehen sie uns kaputt. Von daher seid alle bitte kritisch mit den Öffis ... insbesondere wenn sie ignorant gegenüber Kritik und Interessen von Minderheiten sind ... seid aber trotzdem nett zu den Menschen, die da arbeiten.
