# Zurückweisung Beschwerde:

*Mit dieser Argumentation hat der HR-Rundfunkrat meine Beschwerde als unbegründet zurückgewiesen. Ich werde am Ende ein paar Fakten richtig stellen. Das ist der Text eines mir elektronisch übermittelten Dokuments des HR. Wer einen Tippfehler findet, darf ihn behalten.*

Sehr geehrter  Herr [betalars],
wie angekündigt hat sich der Programmausschuss Fernsehen am 13. September  mit Ihrer Programmbeschwerde  zur Ausstrahlung  des Tatortes "Die Amme" beschäftigt, und ausführlich  über den Sachverhalt  diskutiert. Dabei lag den Mitgliedern des Aus-
Schusses der gesamte Schriftverkehr vor: Ihre Beschwerdeschreiben  vom 16. April sowie der Widersprach vom 2. Juni, Ihre Schreiben an die GVK-Geschäftsstetle, an die Degeto sowie den Pubtikumsrat  des ORF, ebenso das Antwortschreiben des hr-Intendanten  Manfred  Krupp vom 20. Mai 2021 sowie der Maitaustausch  direkt im Vorfeld der Sitzung bezüglich Ihres Wunsches, persönlich angehört zu werden.

Dabei wurde zunächst festgestellt,  dass Ihre Programmbeschwerde in die Zuständigkeit der Aufsichtsgremien  des Hessischen  Rundfunks fällt, auch wenn es sich hier umeine für Außenstehende  eher schwer verständliche,  da komplizierte  formale  Zuständigkeit handelt: 

Die Degeto produziert  und beschafft einen Großteil der fiktionaten  Programme für die ARD, darunter eben auch regelmäßig Tatorte des ORF. Weit die Degetoihren Sitz hier auf dem Gelände des hr in Frankfurt hat, ist der hr letztlich auch für die Behandlung ihrer Beschwerde zuständig.

Nun zur Sache: Ihre Ausführungen  und Ihr außerordentliches  Engagement  in der Sache zeigen, wie sehr dieser Film Sie persönlich verletzt und als Interessenvertreter zutiefst getroffen hat. Ich darf Ihnen versichern, dass dies von den Mitgliedern des Fernsehaus-Schusses wahrgenommen  wurde. Ihre Perspektive nehmen wir ernst und wollen sie weder relativieren noch zurückweisen.  Schließlich spiegelt sich darin Ihre Wahrnehmung als Betroffener.

Allerdings ist die Perspektive des Programmausschusses auf diesen Spielfilm und die kritisierten Darstellungen und Figuren eine gänzlich andere. Aus Sicht des Ausschusses geht es in dem Tatort "Die Amme" um eine psychisch schwer gestörte Person. Esgeht um einen Täter, der gelegentlich auch Frauenkleider trägt. Dabei handelt es sichum eine fiktionale Kunstfigur. wie es im Tatort sehr verschiedene  Personen und Persönlichkeiten als Täter oder Täterinnen gibt. Worum es in diesem ORF-Tatort dagegen überhaupt nicht geht, sind geschtechtsbezogene Identitätsthemen. Es geht auch nicht um plakative oder klischeehafte  diskriminierende  Zuschreibungen  bestimmter  Persönlichkeiten. Ganz im Gegenteil!  Es geht um pure Fiktion und um eine Kunstfigur,  die zufällig bestimmte Verhaltensweisen an den Tag legt, die nun in einem bestimmten Licht wahrgenommen werden sollen und wollen. Damit werden allerdings Assoziationen konstruiert,  die im Zusammenhang  mit dieser fiktionalen  Kunstfigur  und ihren Hand-
lungen zu keinem  Zeitpunkt  intendiert  waren.

Die auch Ihrer Programmbeschwerde zugrundeliegende Grundsatz-Debatte um Identitat und Diskriminierung ist in den Redaktionen ein ständiges Thema mit dem ehrlichen Bemühen um die notwendige und wichtige Sensibilität. Deshalb wird bereits bei Drehbüchem auf Diversität geachtet. Und weil Sie als Interessenvertreter  bereits im Vorfeld der Ausstrahlung Protest angemeldet hatten. hat die Redaktion der Degeto die kommunikative Begleitung in Presse- und social-media-Abteilung noch mal mit besonderer Intensität vorbereitet  und alle Beteiligten  noch einmal sensibilisiert.

In der Sitzung wurde mit Blick auf die Programmbeschwerde von den Ausschussmitgliedern auch das Spannungsfeld  zwischen Kunstfreiheit, der Identität unterschiedlicher Bevölkerungsgruppen und der jeweiligen Lesart der Betroffenen diskutiert. Im Ergebnis ist der Ausschuss aus vorgenannten  Gründen allerdings einhellig anderer Ansieht als Sie und kann keine Stigmatisierung  oder Diskriminierung  der von Ihnen genannten Personengruppen erkennen. Es Liegt auch weder ein Verstoß gegen die Programmgrundsätze  des hr nach dem hr-Gesetz vor noch wurde gegen Jugendschutzbelange verstoßen. Dies ist in der Vorkorrespondenz  bereits ausreichend  und nachvoll-
ziehbar ausgeführt worden.

Deshalb weist der Ausschuss Ihre Programmbeschwerde  einmütig zurück.

Ich möchte der Vollständigkeit  halber darauf hinweisen, dass im Gegensatz zu Ihren Ausführungen  der PubLikumsrat des ORF Ihre Beschwerde ebenfalls abgewiesen  hat und auch von Seiten des ORF eine Freigabe des Filmes ab 12 Jahren vorliegt.

Abschließend darf ich Ihnen versichern, dass wir uns unter Berücksichtigung der Kunstfreiheit  auch weiterhin  bemühen werden,  auf die ausgewogene  und diskriminierungsfreie  Darstellung  von Charakteren  in fiktionalen  Produktionen  zu achten.

Mit freundlichen Grüßen
Daniel Neumann
Vorsitzender Programmausschuss Fernsehen

## Richtigstellungen:
 - Es wurde nicht verständlich entkräftet, inwieweit die Darstellungen nicht Jugendgefährdend seien.
 - Kunstfreiheit bedeutet nicht, dass ich meine grotesken Vorstellungen über "gestörte" Männer in Frauenkleidern, die kleine Kinder entführen und deren Mütter ermorden in der durch die Gemeinschaft getragenen ARD ausgestrahlt werden können.
 - die Beschwerde kritisiert nicht nur implizit transfeindliche Darstellungen, sondern auch explizite Darstellungen, die stigmatisierend gegenüber Menschen mit psychischen Störungen sind.
 - der ORF hat in der Mediathek den Tatort als für Jugendliche unter 16 Jahren ungeeignet eingetragen.
 - es wurde nicht behauptet, dass der Beschwerde recht gegeben wurde.

## Der Vollständigkeit halber ...
 - Ich habe kritisiert, dass in der Sitzung des Programmausschuss Fernsehen keine Menschen mit psychischen Störungen oder keine trans Personen angehört wurden und habe in der gleichen Mail betont, dass ich im Sinne der besseren Nachvollziehbarkeit und auch für kritische Rückfragen dem Ausschuss gern für Rückfragen zur Verfügung stehen würde.
 - Es im vorhinein ausführlichen Schrift- und Telefonverkehr gegeben, in dem mir gegenüber im Bezug auf die transfeindlichkeit der Darstellungen teilweise auch recht gegeben wurde.
 - Es war tatsächlich ein größeres Unterfangen heraus zu finden, wer eigentlich genau für diese Beschwerde in Deutschland zuständig ist, weil die ARD ein Gemeinschaftsangebot der Landesrundfunkhäuser ist (sprich mdr, NDR, wdr usw sind jeweils für die eigenen Angebote zuständig), dieser Tatort jedoch durch den ORF produziert wurde.
